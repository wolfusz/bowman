package engine.utils;

import java.awt.event.MouseEvent;

public interface Clickable {
	void onClick(MouseEvent e);
}
