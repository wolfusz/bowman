package engine.utils;

import java.awt.Shape;
import java.awt.geom.Area;

public interface Collidable {
	void die();
	
	default Shape getBounds() {
		return null;
	}
	
	default boolean isColliding(Collidable other) {
		if (getBounds() != null && other.getBounds() != null) {
			Area area = new Area(getBounds());
			area.intersect(new Area(other.getBounds())); 
			return !area.isEmpty();
		}
		return false;
	}
	
	default boolean isColliding(Shape other) {
		if (getBounds() != null && other.getBounds() != null) {
			Area area = new Area(getBounds());
			area.intersect(new Area(other)); 
			return !area.isEmpty();
		}
		return false;
	}
	
	default boolean onCollide(Collidable other) {
		return false;
	}
}
