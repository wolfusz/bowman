package engine.utils;

import java.awt.geom.Ellipse2D;

public class Utils {
	public static final float DEGTORAD = (float) (Math.PI/180);
	public static final float RADTODEG = (float) (180/Math.PI);
	
	public static Point<Float> getVelocityByAngle(float force, float angle) {
		return new Point<Float>(force*(float)Math.cos(angle), force*(float)Math.sin(angle));
	}

	public static boolean pointInRange(Point<Integer> point, Point<Integer> center, int range) {
		return new Ellipse2D.Double(center.x - range, center.y - range, range*2, range*2).contains(point.x, point.y);
	}
}
