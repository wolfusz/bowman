package engine.utils;

import engine.entity.Entity;

public interface Attachable {
	boolean isAttached();
	void attach(Entity entity);
	void attachAt(Entity entity, Point<Integer> point);
}
