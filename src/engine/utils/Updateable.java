package engine.utils;

public interface Updateable {
	void update();
}
