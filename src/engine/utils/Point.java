package engine.utils;

public class Point<T> {
	public T x;
	public T y;
	
	public Point(T x, T y) {
		this.x = x;
		this.y = y;
	}
	public Point() { this(null, null); }
	
	public void set(T x, T y) {
		this.x = x;
		this.y = y;
	}
	
	public String toString() {
		return "{"+x+","+y+"}";
	}
}
