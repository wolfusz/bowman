package engine.utils;

import java.awt.Graphics2D;

public interface Renderable {
	void render(Graphics2D g);
	default void renderHalf(Graphics2D g) {}
}
