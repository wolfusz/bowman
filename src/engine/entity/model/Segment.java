package engine.entity.model;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import engine.entity.Entity;
import engine.utils.Point;
import engine.utils.Renderable;
import engine.utils.Updateable;
import engine.utils.Utils;

public class Segment extends Entity implements Renderable, Updateable {
	
	public enum Type {
		Line,
		Oval
	};
	
	public Segment parent;
	public int length;
	public float angle;
	public Float minAngle, maxAngle;
	public Type type;
	
	public Segment(Type type, Segment parent, int x, int y, int length, float angle, Float minAngle, Float maxAngle) {
		super(x, y);
		this.parent = parent;
		this.length = length;
		this.angle = angle;
		this.minAngle = minAngle;
		this.maxAngle = maxAngle;
		this.type = type;
	}
	// constructors without parent
	public Segment(Type type, int x, int y, int length, int angle, Integer minAngle, Integer maxAngle) {
		this(type, null, x, y, length, (angle * Utils.DEGTORAD), (minAngle != null ? (float)(minAngle * Utils.DEGTORAD) : null), (maxAngle != null ? (float)(maxAngle * Utils.DEGTORAD) : null));
	}
	public Segment(Type type, int x, int y, int length, int angle) {
		this(type, null, x, y, length, (angle * Utils.DEGTORAD), null, null);
	}
	public Segment(Type type, int x, int y, int length) {
		this(type, null, x, y, length, 0f, null, null);
	}
	// constructors with parent
	public Segment(Type type, Segment parent, int length, int angle, Integer minAngle, Integer maxAngle) {
		this(type, parent, 0, 0, length, (angle * Utils.DEGTORAD), (minAngle != null ? (float)(minAngle * Utils.DEGTORAD) : null), (maxAngle != null ? (float)(maxAngle * Utils.DEGTORAD) : null));
	}
	public Segment(Type type, Segment parent, int length, int angle) {
		this(type, parent, 0,0, length, (angle * Utils.DEGTORAD), null, null);
	}
	public Segment(Type type, Segment parent, int length) {
		this(type, parent, 0,0, length, 0f, null, null);
	}

	
	public void  setAngle(float angle) { this.angle = angle; }
	public void  setAngle(int angle)   { setAngle(angle * Utils.DEGTORAD); }
	public float getAngle()            { return this.angle; }

	public float getMinAngle()            { return this.minAngle; }
	public void  setMinAngle(float angle) { this.minAngle = angle; }
	public float getMaxAngle()            { return this.maxAngle; }
	public void  setMaxAngle(float angle) { this.maxAngle = angle; }
	
	public boolean hasParent() { return parent != null; }
	public Segment getParent() { return this.parent; }

	public Point<Integer> getPos() { return new Point<Integer>(getX(), getY()); }
	@Override public int getX() {
//		if (hasParent())
//			return parent.getPin().x + (int)(super.getX() * Math.cos(parent.getAngle()));
		return super.getX();
	}
	@Override public int getY() {
//		if (hasParent())
//			return parent.getPin().y + (int)(super.getY() * Math.sin(parent.getAngle()));
		return super.getY();
	}
	
	public AffineTransform getTransform() {
		AffineTransform t = new AffineTransform();
		t.translate(getX(), getY());
		t.rotate(getAngle());
		return t;
	}
	
	public void position(Segment t) {
		setX( t.getPin().x );
		setY( t.getPin().y );
	}
	
	public Point<Integer> getPin() {
		double x = getX() + Math.cos(angle) * length;
		double y = getY() + Math.sin(angle) * length;
		return new Point<Integer>((int)x, (int)y);
	}
	
	public Float pointAt(Point<Integer> target) {
		if (target == null) return null;
		int dx = target.x - getX();
		int dy = target.y - getY();
		return (float) Math.atan2(dy, dx);
	}
	
	public Point<Integer> reach(Point<Integer> target) {
		if (target == null) return null;
		Float angle = pointAt(target);

		/*
		if (hasParent()) {
			float dAngle = parent.getAngle() - angle;
			if (minAngle != null && dAngle < minAngle)
				angle = null;
			if (maxAngle != null && dAngle > maxAngle)
				angle = null;
		} else {
			if (minAngle != null && angle < minAngle)
				angle = null;
			if (maxAngle != null && angle > maxAngle)
				angle = null;
		}
		/* /
		if (minAngle != null) {
			float dAngle = minAngle;
			if (hasParent())
				dAngle += parent.getAngle();
			if (angle < dAngle)
				angle = dAngle;
		}
		if (maxAngle != null) {
			float dAngle = maxAngle;
			if (hasParent())
				dAngle += parent.getAngle();
			if (angle > dAngle)
				angle = dAngle;
		}
		//*/
		
		if (angle != null)
			this.angle = angle;
		
		int w = getPin().x - getX();
		int h = getPin().y - getY();
		return new Point<Integer>(target.x - w, target.y - h);
	}
	
	// inwersja kinematyczna (imitacja poruszania r�k�, nog� etc.)
	public Float IK(Segment B, Point<Integer> target) {
		if (B == null) return null;
		target = reach(target);
		B.reach(target);
		position(B);
		return angle;
	}
	
	public Float IK(Point<Integer> target) {
		if (hasParent())
			return IK(parent, target);
		return pointAt(target);
	}

	@Override
	public void render(Graphics2D g) {
		g.setColor(Color.BLACK);
		g.transform(getTransform());
		switch (type) {
		case Line: {
			g.fillRoundRect(-5, -5, length+10, 10, 10, 10);
		} break;
		case Oval: {
			int halfLength = length/2;
			g.fillOval(-halfLength, -halfLength, length, length);
		} break;
		}
	}

	@Override
	public void update() {
		if (parent != null) {
			position(parent);
		}
	}
}
