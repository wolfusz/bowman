package engine.entity.model;

import java.util.HashMap;
import java.util.LinkedList;

import engine.Engine;
import engine.entity.model.Segment.Type;
import engine.utils.Updateable;

public class Model implements Updateable {
	
	private HashMap<String, Segment> segments;
	
	public Model(int x, int y) {
		segments = new HashMap<>();
		init(x, y);
	}
	
	public void init(int x, int y) {
		clear();
		segments.put("placeholder", new Segment(Type.Line, x, y, 0, -90));
	}
	
	public void clear() {
		if (segments != null)
			segments.clear();
	}
	
	public void setPos(int x, int y) {
		// ustaw model postaci na odpowiedniej pozycji X
		get("placeholder").setX(x);
		
		// a teraz ustaw na pozycji Y tak, �eby najni�szy punkt modelu
		// by� na danej pozycji y, wi�c trzeba przesun�� "placeholder"
		int pY = get("placeholder").getY();
		int dY = 0;
		int sY;
		
		// dla wszystkich segment�w
		Segment segment;
		for (String ID : segments.keySet()) {
			if (ID == "placeholder") continue;
			segment = segments.get(ID);
			
			System.out.println("###  "+ID);
			System.out.println("posY: " + segment.getY());
			System.out.println("pinY: " + segment.getPin().y);
			
			// sprawd� co by�o ni�ej - pozycja segmentu czy jego zako�czenie
			sY = Math.max( segment.getY(), segment.getPin().y);
			System.out.println("sY:  " + sY);
			
			// sprawd� czy segment znajduje si� najdalej od "placeholder"
			// i oblicz o ile trzeba przesun�� "placeholder" je�li tak
			if (sY - pY > dY)
				dY = sY - pY;
			System.out.println("dY:  " + dY);
		}
		System.out.println("dY: "+dY);
		System.out.println("y-dY: "+(y-dY));
		get("placeholder").setY(y - dY);
		update();
	}

	@Override
	public void update() {
		for (Segment segment : segments.values())
			segment.update();
	}
	
	//## operacje na segmentach
	public Segment addSegment(String ID, Segment segment) {
		if (segments == null)
			segments = new HashMap<>();
		segments.put(ID, segment);
		Engine.getWorld().addEntity(segment);
		return segment;
	}
	
	public Segment get(String ID) {
		return segments.get(ID);
	}
	
	// pobranie dzieci dla danego segmentu
	public LinkedList<Segment> getChildrens(Segment parent) {
		LinkedList<Segment> result = new LinkedList<>();
		for (Segment segment : segments.values())
			if (segment.hasParent() && segment.parent == parent)
				result.add(segment);
		return result;
	}

//	// pobranie segment�w, kt�re nie s� rodzicami
//	public LinkedList<Segment> getLastRootChildrens() {
//		LinkedList<Segment> result = new LinkedList<>();
//		boolean isParent;
//		// dla ka�dego elementu
//		for (Segment parent : segments.values()) {
//			isParent = false;
//			// sprawdzamy czy nie jest rodzicem jakiego� innego elementu
//			for (Segment segment : segments.values())
//				// tutaj sprawdzamy jeszcze, czy nie wykonujemy dzia�a�
//				// dla tego samego segmentu
//				if (segment != parent) {
//					// je�li segment ma rodzica i tym rodzicem jest segment z pierwszej p�tli
//					// to poinformuj, �e jest rodzicem i wyjd� z tej p�tli
//					if (segment.hasParent() && segment.parent == segment) {
//						isParent = true;
//						break;
//					}
//				}
//			// nie znaleziono segmentu dla kt�rego ten segment by�by rodzicem
//			// wi�c dodaj go do wyniku
//			if (!isParent)
//				result.add(parent);
//		}
//		return result;
//	}
}
