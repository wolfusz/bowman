package engine.entity;

import engine.entity.model.Model;

public class Living extends Entity {
	
	protected Model model;

	public Living(int x, int y) {
		super(x, y);
		// stworzenie cia�a istoty
		model = new Model(x, y);
	}
	
	public Model getModel() { return model; }

}
