package engine.entity;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;

import engine.Engine;
import engine.utils.Attachable;
import engine.utils.Collidable;
import engine.utils.Point;
import engine.utils.Renderable;
import engine.utils.Updateable;
import engine.utils.Utils;

public class Target extends Entity implements Updateable, Renderable, Collidable {

	public Target(int x, int y) {
		super(x, y);
	}

	@Override
	public void die() {
		setHealth(0);
	}
	
	public AffineTransform getTransform() {
		AffineTransform transform = new AffineTransform();
		transform.translate(getX(), getY());
		transform.rotate(getAngle());
		return transform;
	}
	
	public Shape getRed()    { return new Ellipse2D.Double(-10, -20, 20, 40); }
	public Shape getYellow() { return new Ellipse2D.Double(-20, -40, 40, 80); }
	public Shape getBlue()   { return new Ellipse2D.Double(-30, -50, 60, 100); }
	
	public Shape getRedTransformed()    { return getTransform().createTransformedShape(getRed()); }
	public Shape getYellowTransformed() { return getTransform().createTransformedShape(getYellow()); }
	public Shape getBlueTransformed()   { return getTransform().createTransformedShape(getBlue()); }

	@Override
	public void render(Graphics2D g) {
		g.translate(getX(), getY());
		g.rotate(10 * Utils.DEGTORAD);
		
		g.setColor(Color.BLUE);
		g.fill(getBlue());		
		g.setColor(Color.YELLOW);
		g.fill(getYellow());		
		g.setColor(Color.RED);
		g.fill(getRed());
	}
	
	@Override
	public void renderHalf(Graphics2D g) {
		
		g.translate(getX(), getY());
		g.rotate(10 * Utils.DEGTORAD);
		g.setClip(new Rectangle(0, -100, 101, 201));
		
		g.setColor(Color.BLUE);
		g.fill(getBlue());		
		g.setColor(Color.YELLOW);
		g.fill(getYellow());		
		g.setColor(Color.RED);
		g.fill(getRed());
		
		g.dispose();
	}
	
	@Override
	public Shape getBounds() {
		return getTransform().createTransformedShape(getBlue());
	}
	
	@Override
	public void update() {
		if (!isDead())
			for (Collidable collidable : Engine.getWorld().getCollidablesButNot(this)) {
				// sprawdzamy czy wyst�puje kolizja
				if (collidable instanceof Attachable)
					if (((Attachable) collidable).isAttached())
						continue;
				if (collidable.isColliding(getBlueTransformed())) {
					// je�li tak, to wywo�aj funkcj� i wyjd� z p�tli
					onCollide(collidable);
					collidable.onCollide(this);
					break;
				}
			}
	}
	
	@Override
	public boolean onCollide(Collidable other) {
		if (other.isColliding( getRedTransformed() )) {
			Engine.getInstance().score += 25;
		} else if (other.isColliding( getYellowTransformed() )) {
			Engine.getInstance().score += 15;
		} else if (other.isColliding( getBlueTransformed() )) {
			Engine.getInstance().score += 5;
		}
		if (other instanceof Entity && other instanceof Attachable)
			((Attachable) other).attachAt(this, new Point<Integer>(((Entity)other).getX() - getX(), ((Entity)other).getY() - getY()));
		return true;
	}

}
