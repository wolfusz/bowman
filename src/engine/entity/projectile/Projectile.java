package engine.entity.projectile;

import engine.entity.Entity;
import engine.utils.Collidable;
import engine.utils.Point;
import engine.utils.Updateable;

public class Projectile extends Entity implements Updateable, Collidable {
	
	private int width;
	private int height;
	private Point<Float> force;
	private float angle;

	public Projectile(int x, int y, int width, int height, float angle, Point<Float> force) {
		super(x, y);
		this.width = width;
		this.height = height;
		this.angle = angle;
		this.force = force;
	}

	@Override
	public void die() {
		setHealth(0);
	}
	
	
	public int getWidth()  { return this.width; }
	public int getHeight() { return this.height; }
	
	public void         applyForce(Point<Float> force) { this.force = force; }
	public Point<Float> getForce()                     { return this.force; }
	
	public void  setAngle(float angle) { this.angle = angle; }
	public float getAngle()            { return this.angle; }
	
	
	public void applyForceToPosAndAngle() {
		if (force != null) {
			setX( (int) (getX() + force.x) );
			setY( (int) (getY() + force.y) );
			setAngle( (float) Math.atan2(force.y, force.x) );
		}
	}
	
	public void doForce() {
		applyForceToPosAndAngle();
		// make force slowly decrease
		if (force != null) {
			force.x *= .99f;
			force.y *= .99f;
			if (force.x < .01f && force.x > -.01f && force.y < .01f && force.y > .01f)
				force = null;
		}
	}

	@Override
	public void update() {
		if (! isDead()) {
			doForce();
			setHealth(getHealth() - .01f);
		}
	}

}
