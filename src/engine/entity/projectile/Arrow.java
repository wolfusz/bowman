package engine.entity.projectile;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;

import engine.entity.Entity;
import engine.utils.Attachable;
import engine.utils.Collidable;
import engine.utils.Point;
import engine.utils.Renderable;
import engine.utils.Utils;

public class Arrow extends Projectile implements Renderable, Collidable, Attachable {
	
	public Arrow(int x, int y, int width, int height, float angle, Point<Float> force) {
		super(x, y, width, height, angle, force);
		attachedTo = null;
		attachedAt = null;
	}
	public Arrow(int x, int y, int width, int height, Point<Float> force) {
		this(x, y, width, height, force != null ? (float) Math.atan2(force.y, force.x) : 0f, force);
	}
	public Arrow(int x, int y, int width, int height, int angle) {
		this(x, y, width, height, (angle * Utils.DEGTORAD), null);
	}
	public Arrow(int x, int y, int width, int height) {
		this(x, y, width, height, 0f, null);
	}
	
	
	@Override
	public Rectangle getBounds() {
		return new Rectangle(getX()-(getWidth()/2), getY()-(getHeight()/2), getWidth(), getHeight());
	}
	
	//## przyczepianie do element�w
	private Entity attachedTo;
	private Point<Integer> attachedAt;
	
	@Override
	public boolean isAttached() {
		return attachedTo != null;
	}
	
	@Override
	public void attach(Entity entity) {
		attachedTo = entity;
		if (attachedTo == null)
			attachedAt = null;
	}

	@Override
	public void attachAt(Entity entity, Point<Integer> point) {
		attach(entity);
		attachedAt = point;
	}
	

	@Override
	public void render(Graphics2D g) {
		AffineTransform savedTransform = g.getTransform();
		
		g.translate(getX(), getY());
		g.rotate(getAngle());
		
		// rysuj strza��
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, getWidth(), getHeight());
		
		// rysuj grot
		g.setColor(Color.DARK_GRAY);
		Polygon grot = new Polygon();
		grot.addPoint(getWidth()-getHeight(), -getHeight());
		grot.addPoint(getWidth()-getHeight(), getHeight()*2);
		grot.addPoint(getWidth()+getHeight()*2, getHeight()/2);
		g.fillPolygon(grot);
		
		if (isAttached() && attachedTo instanceof Renderable) {
			g.setTransform(savedTransform);
			((Renderable)attachedTo).renderHalf((Graphics2D)g.create());
		}
	}

	@Override
	public void doForce() {
		applyForceToPosAndAngle();
		// make force slowly decrease
		if (getForce() != null) {
			float fx = getForce().x * 1.005f;
			float fy = getForce().y + .981f;
			if (fx < .01f && fx > -.01f && fy < .01f && fy > -.01f)
				applyForce(null);
			else
				applyForce(new Point<>(fx, fy));
		}
	}
	
	@Override
	public void update() {
		if (attachedTo != null) {
			if (attachedAt != null)
				setPos(attachedTo.getX() + attachedAt.x, attachedTo.getY() + attachedAt.y);
			else
				setPos(attachedTo.getX(), attachedTo.getY());
		} else
			super.update();
	}
}
