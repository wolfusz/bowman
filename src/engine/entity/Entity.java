package engine.entity;

import engine.utils.Point;
import engine.utils.Utils;

public class Entity {
	
	//# parametry
	private float health;
	
	private int x;
	private int y;
	
	private float angle;
	
	//# konstruktory
	public Entity(int x, int y, float angle, float health) {
		// pozycja postaci
		this.x = x;
		this.y = y;
		
		this.angle = angle;
		
		// na starcie zdrowie = 100%
		this.health = health;
	}
	public Entity(int x, int y, float angle) {
		this(x, y, angle, 1f);
	}
	public Entity(int x, int y, int angle) {
		this(x, y, (angle * Utils.DEGTORAD), 1f);
	}
	public Entity(int x, int y) {
		this(x, y, 0f, 1f);
	}
	
	//# gettery i settery
	public void  setHealth(float hp) { this.health = hp; }
	public float getHealth()         { return this.health; }
	
	public void  setAngle(float angle) { this.angle = angle; }
	public float getAngle()            { return this.angle; }
	
	public void setPos(int x, int y) {
		this.x = x;
		this.y = y;
	}
	public Point<Integer> getPos() { return new Point<Integer>(getX(), getY()); }
	
	public void setX(int x) { this.x = x; }
	public int  getX()      { return x; }
	public void setY(int y) { this.y = y; }
	public int  getY()      { return y; }
	
	//# przesuni�cie postaci o jaki� wektor
	public void move(int dx, int dy) {
		this.setX( this.getX() + dx );
		this.setY( this.getY() + dy );
	}
	
	//# funkcje informuj�ce o stanie
	public boolean isDead() { return !(getHealth() > 0); }
}
