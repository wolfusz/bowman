package engine.entity;

import engine.entity.model.Model;
import engine.entity.model.Segment;
import engine.entity.model.Segment.Type;
import engine.utils.Updateable;

public class Human extends Living implements Updateable {
	
	public Human(int x, int y) {
		super(x, y);
		initModel();
	}
	public Human() {
		this(0, 0);
	}
	
	public void initModel() {
		// sprawd� czy model zosta� stworzony
		if (model == null)
			// je�li nie, to go utw�rz
			model = new Model(getX(), getY());
		else
			// w przeciwnym wypadku wyczy�� poprzedni model
			model.init(getX(), getY());

		// stworzenie modelu cz�owieka
		// tu��w + g�owa
		model.addSegment("spine", new Segment(Type.Line, model.get("placeholder"), 60, -90));
		model.addSegment("neck", new Segment(Type.Line, model.get("spine"), 10, -80));
		model.addSegment("head", new Segment(Type.Oval, model.get("neck"), 23, -90));
		// nogi
		model.addSegment("legL", new Segment(Type.Line, model.get("placeholder"), 40, 100));
		model.addSegment("legR", new Segment(Type.Line, model.get("placeholder"), 40, 70));
		model.addSegment("calfL", new Segment(Type.Line, model.get("legL"), 45, 110));
		model.addSegment("calfR", new Segment(Type.Line, model.get("legR"), 45, 80));
		// r�ce
		model.addSegment("armL", new Segment(Type.Line, model.get("spine"), 30, 100, -160, 180));
		model.addSegment("armR", new Segment(Type.Line, model.get("spine"), 30, 95));
		model.addSegment("forearmL", new Segment(Type.Line, model.get("armL"), 30, 70, 0, 180));
		model.addSegment("forearmR", new Segment(Type.Line, model.get("armR"), 30, 85, 0, 180));//, 10, 90));
	}
	
	@Override
	public void update() {
		// wykonuj kod tylko je�li posta� jest �ywa
		if (! isDead()) {
			model.update();
		}
	}

}
