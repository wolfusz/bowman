package engine.entity;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;

import engine.Engine;
import engine.entity.model.Segment;
import engine.entity.weapon.Bow;
import engine.entity.weapon.ChargeableWeapon;
import engine.entity.weapon.Shootable;
import engine.entity.weapon.Weapon;
import engine.utils.Point;
import engine.utils.Utils;

public class Player extends Human {
	
	private Color color;
	
	public Weapon weapon;
	
	public Player(int x, int y) {
		super(x, y);
		this.color = Color.BLACK;
		this.weapon = null;
	}
	public Player() { this(0, 0); }
	
	public void  setColor(Color color) { this.color = color; }
	public Color getColor()            { return this.color; }
	
	public void setPos(int x, int y) {
		// ustaw pozycj�
		setX(x);
		setY(y);
		model.setPos(x, y);
	}
	
	public void giveWeapon(Weapon weapon) {
		Engine.getWorld().removeEntity(this.weapon);
		this.weapon = weapon;
		Engine.getWorld().addEntity(this.weapon);
	}
	
	@Override
	public void update() {
		//wykonaj kod z klasy Player
		if (! isDead()) {
			Segment target;
			float diffAngle, deltaAngle, newAngle;

			// wyceluj lew� r�k� w stron� kursora myszy
			target = model.get("armL");
			diffAngle = target.getAngle() - model.get("forearmL").getAngle();
			newAngle = target.pointAt(Engine.getInstance().lastMousePos) + diffAngle;
			deltaAngle = newAngle - target.getAngle();
			target.setAngle(newAngle);
			for (Segment child : model.getChildrens(target))
				child.setAngle(child.getAngle() + deltaAngle);
			
			if (weapon != null) {
				AffineTransform transform = new AffineTransform();
				
				// ustaw bro� na pozycji lewej r�ki
				Segment segment = model.get("forearmL");
				Point<Integer> point = segment.getPin();
				transform.translate(point.x, point.y);
				transform.rotate(segment.getAngle());
				weapon.setPos((int)transform.getTranslateX(), (int)transform.getTranslateY());
				weapon.setAngle(segment.getAngle());
				
				if (weapon instanceof Bow) {
					// na podstawie lewej r�ki wyznacz pozycj� �rodka ci�ciwy �uku
					target = model.get("forearmL");
					transform = new AffineTransform();
					transform.translate(weapon.getX(), weapon.getY());
					transform.rotate(weapon.getAngle());
					transform.translate(-5-((Bow)weapon).chargeStrength(), 0);
					
					// wypozycjonuj praw� r�k� do ci�ciwy �uku
					if (((Bow) weapon).isCharging()) {
						model.get("armR").setAngle((float)(model.get("armL").getAngle() - Math.PI/2));
						model.get("forearmR").IK(new Point<Integer>((int)transform.getTranslateX(), (int)transform.getTranslateY()));
					} else {
						model.get("armR").setAngle(95);
						model.get("forearmR").setAngle(90);
					}
				}
			}
			
			// aktualizacja modelu postaci
			model.update();
		}
	}
	
	public void mousePressed(MouseEvent e) {
		if (weapon != null && weapon instanceof ChargeableWeapon) {
			ChargeableWeapon chargeableWeapon = (ChargeableWeapon)weapon;
			if (e.getButton() == MouseEvent.BUTTON1)
				chargeableWeapon.setChargeStep(1);
			else if (e.getButton() == MouseEvent.BUTTON3)
				chargeableWeapon.setChargeStep(3);
			chargeableWeapon.charge();
		}
	}
	
	public void mouseReleased(MouseEvent e) {
		if (weapon != null) {
			if (weapon instanceof Shootable) {
				// wystrzel z pozycji broni
				((Shootable)weapon).shoot(
						// oblicz si�� z jak� wystrzeliwujemy z broni
						Utils.getVelocityByAngle(
								( // je�li bro� mo�na �adowa�
									weapon instanceof ChargeableWeapon ?
									  // to wystrzel z si�� r�wn� na�adowaniu broni
										((ChargeableWeapon)weapon).chargeStrength()
									: // w przeciwnym wypadku moc broni * 2
										weapon.damage*2
								),
								// bazuj�c na obrocie lewego przedramienia
								model.get("forearmL").getAngle()
							)
					);
			}
			
			// je�li bro� mo�na �adowa�
			if (weapon instanceof ChargeableWeapon) {
				ChargeableWeapon chargeableWeapon = (ChargeableWeapon)weapon;
				// sprawd� czy bro� jest aktualnie �adowana
				if (chargeableWeapon.isCharging())
					// zatrzymaj �adowanie broni
					chargeableWeapon.stopCharging();
			}
		}
	}
}
