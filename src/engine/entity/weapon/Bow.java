package engine.entity.weapon;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import engine.Engine;
import engine.entity.projectile.Arrow;
import engine.utils.Point;
import engine.utils.Renderable;
import engine.utils.Utils;

public class Bow extends ChargeableWeapon implements Renderable, Shootable {

	private Arrow arrow;
	
	public Bow(int x, int y, float angle, int damage) {
		super(x, y, angle, damage, 40);
		this.arrow = null;
	}
	public Bow(int x, int y, int angle, int damage) {
		this(x, y, (angle * Utils.DEGTORAD), damage);
	}
	public Bow(int x, int y, int angle) {
		this(x, y, angle, 0);
	}

	@Override
	public void render(Graphics2D g) {
		// charging arrow / strza�a
		if (arrow != null)
			arrow.render((Graphics2D)g.create());
		
		g.translate(getX(), getY());
		g.rotate(getAngle());
		
		// bow / �uk
		g.setColor(new Color(1f,.5f,0f));
		g.setStroke(new BasicStroke(6f));
		g.drawArc(-10, -45, 20, 90, -100, 200);
		
		// chord / ci�ciwa
		g.setColor(Color.BLACK);
		g.setStroke(new BasicStroke(0));
		g.drawLine(-5-chargeStrength(), 0, -5, -45);
		g.drawLine(-5-chargeStrength(), 0, -5, +45);
	}
	
	@Override
	public void update() {
		super.update();
		if (arrow != null) {
			// przesu� strza�� na pozycji aktualnego odchy�u ci�ciwy
			AffineTransform transform = new AffineTransform();
			transform.translate(getX(), getY());
			transform.rotate(getAngle());
			transform.translate(-5-chargeStrength(), 0);
			arrow.setPos((int)transform.getTranslateX(), (int)transform.getTranslateY());
			arrow.setAngle(getAngle());
		}
	}
	
	@Override
	public void charge() {
		super.charge();
		// za�aduj strza�� na ci�ciw�
		arrow = new Arrow(getX(), getY(), 60, 2);
	}

	@Override
	public void shoot(Point<Float> velocity) {
		if (arrow == null)
			arrow = new Arrow(getX(), getY(), 60, 2);
		arrow.applyForce(velocity);
		Engine.getWorld().addEntity(arrow);
		stopCharging();
		arrow = null;
	}
}
