package engine.entity.weapon;

import engine.entity.Entity;
import engine.utils.Utils;

public class Weapon extends Entity {
	
	public int damage;

	public Weapon(int x, int y, float angle, int damage) {
		super(x, y, angle);
		this.damage = damage;
	}
	public Weapon(int x, int y, int angle, int damage) {
		this(x, y, (angle * Utils.DEGTORAD), damage);
	}
	public Weapon(int x, int y, int angle) {
		this(x, y, angle, 0);
	}

}
