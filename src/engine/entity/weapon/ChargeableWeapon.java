package engine.entity.weapon;

import engine.utils.Updateable;
import engine.utils.Utils;

public class ChargeableWeapon extends Weapon implements Updateable {

	public ChargeableWeapon(int x, int y, float angle, int damage, int maxChargeStrength) {
		super(x, y, angle, damage);
		this.isCharging = false;
		this.chargeStrength = 0;
		this.chargeStep = 1;
		this.maxChargeStrength = maxChargeStrength;
	}
	public ChargeableWeapon(int x, int y, int angle, int damage, int maxChargeStrength) {
		this(x, y, (angle * Utils.DEGTORAD), damage, maxChargeStrength);
	}
	public ChargeableWeapon(int x, int y, int angle, int maxChargeStrength) {
		this(x, y, angle, 0, maxChargeStrength);
	}

	// weapon charging
	protected boolean isCharging;
	public boolean isCharging() { return isCharging; }
	
	protected int chargeStrength;
	protected int maxChargeStrength;
	public int chargeStrength() { return chargeStrength; }
	
	protected int chargeStep;
	public void setChargeStep(int step) {
		this.chargeStep = step;
	}
	
	public void charge() {
		isCharging = true;
		chargeStrength = 0;
	}
	public void stopCharging() {
		isCharging = false;
		chargeStrength = 0;
	}
	
	@Override
	public void update() {
		if (isCharging()) {
			if (chargeStrength < maxChargeStrength)
				chargeStrength += chargeStep;
		}
	}
}
