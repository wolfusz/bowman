package engine.entity.weapon;

import engine.utils.Point;

public interface Shootable {
	default void shoot(int startX, int startY, Point<Float> velocity) {};
	default void shoot(Point<Float> velocity) {};
}
