package engine;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.util.Iterator;
import java.util.LinkedList;

import engine.entity.Entity;
import engine.utils.Collidable;
import engine.utils.Point;
import engine.utils.Renderable;
import engine.utils.Updateable;

public class World implements Updateable, Renderable {

	private Point<Integer> size; // rozmiar �wiata x,y to 2*size.x, 2*size.y
	private LinkedList<Entity> entities;
	
	public World(int width, int height) {
		size = new Point<>(width, height);
		
		entities = new LinkedList<>();
	}
	
	public int getWidth()  { return size.x; }
	public int getHeight() { return size.y; }
	
	//## rzeczy dotycz�ce istot w �wiecie
	// dodanie/zusuni�cie istoty do/z naszego �wiata
	public void addEntity(Entity entity) {
		entities.add(entity);
	}
	public void removeEntity(Entity entity) {
		Iterator<Entity> iterator = entities.iterator();
		// dla ka�dego elementu
		while (iterator.hasNext())
			// sprawdzamy czy jest taki jak podany w parametrze
			if (iterator.next() == entity) {
				// usuwamy i wychodzimy z p�tli
				iterator.remove();
				break;
			}
	}
	// usuni�cie wszystkich istot ze �wiata
	public void clearEntities() {
		if (entities != null)
			entities.clear();
	}
	// sprawdzenie czy istota istnieje w tym �wiecie
	public boolean contains(Entity entity) { return entities != null && entities.contains(entity); }
	
	// sprawdzanie czy punkt znajduje si� w naszym �wiecie
	public boolean pointInBounds(Point<Integer> point) {
		return new Rectangle(-size.x, -size.y, size.x*2, size.y*2).contains(point.x, point.y);
	}

	// pobranie wszystkich istot, z kt�rymi mo�na wywo�a� kolizj�
	public LinkedList<Collidable> getCollidables() {
		LinkedList<Collidable> result = new LinkedList<>();
		for (Entity entity : entities)
			if (entity instanceof Collidable)
				result.add((Collidable) entity);
		return result;
	}

	// jak wy�ej, tylko z wy��czeniem wybranej istoty
	public LinkedList<Collidable> getCollidablesButNot(Collidable collidable) {
		LinkedList<Collidable> result = new LinkedList<>();
		for (Entity entity : entities)
			if (entity instanceof Collidable && entity != collidable)
				result.add((Collidable) entity);
		return result;
	}

	@Override
	public void render(Graphics2D g) {
		// przesuni�cie canvas'u na �rodek ekranu
		g.translate(getWidth()/2, getHeight()/2);
		
		// rysuj pod�og�
		g.setColor(Color.GREEN);
		g.fillRect(-getWidth(), 0, getWidth()*2, getHeight());
		
		AffineTransform savedTransform = g.getTransform();
		// rysuj obiekty
		for (Entity entity : entities) {
			// kt�re mo�na narysowa�
			if (entity instanceof Renderable) {
				((Renderable)entity).render(g);
				g.setTransform(savedTransform);
			}
		}
	}

	@Override
	public void update() {
		// u�ywam tutaj iterator, bo za pomoc� p�tli foreach
		// nie mo�naby by�o usun�� obiektu z listy :)
		Iterator<Entity> iterator = entities.iterator();
		Entity entity;
		while (iterator.hasNext()) {
			entity = iterator.next();
			
			// sprawd� czy obiekt jest �ywy, je�li nie, to go usu�
			if (entity.isDead()) {
				iterator.remove();
				continue;
			}
			
			// i aktualizuj je�li jest mo�liwo��
			if (entity instanceof Updateable)
				((Updateable)entity).update();
		}
	}
}
