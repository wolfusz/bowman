package engine;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;

import javax.swing.JPanel;

import GUI.Button;
import GUI.Menu;
import engine.entity.Player;
import engine.entity.Target;
import engine.entity.weapon.Bow;
import engine.utils.Point;
import engine.utils.Renderable;
import engine.utils.Updateable;

public class Engine {
	
	//## klasa z tylko jedn� instancj�
	private static Engine instance = null;
	public static Engine getInstance() {
		if (instance == null)
			instance = new Engine();
		return instance;
	}
	
	// funkcje wywo�uj�ce prywatne funkcje instancji silnika // mo�na to potraktowa� jako skr�ty :)
	public static State getState() {
		return getInstance().state;
	}
	public static World getWorld() {
		return getInstance().world;
	}
	

	//## rzeczy dotycz�ce bezpo�rednio silnika
	//# zwi�zane z interfejsem u�ytkownika
	private State state;
	private Menu menu;
	
	public Point<Integer> lastMousePos;
	public int score;
	
	//#zwi�zane ze �wiatem gry
	private World world;
	private Player player;
	
	
	// konstruktor ukryty, przez co nikt nie wywo�a konstruktora
	// pr�cz tej klasy lub klasy dziedzicz�cej po niej
	protected Engine() {}
	
	// funkcja inicjalizuj�ca silnik gry
	public void init(JPanel renderingPanel) {
		// obs�uga zdarze� myszy
		MouseAdapter mouseAdapter = new MouseAdapter() {
			@Override public void mousePressed(MouseEvent e) {
				// je�li menu widoczne
				if (state == State.MENU) {
					// to powiedz mu, �e by�o klikni�cie mysz�
					menu.mousePressed(e);
				} else {
					player.mousePressed(e);
				}
			}
			
			@Override public void mouseReleased(MouseEvent e) {
				if (state == State.PLAYING)
					player.mouseReleased(e);
			}
			
			@Override public void mouseDragged(MouseEvent e) {
				getMousePos(e);
			}
			
			@Override public void mouseMoved(MouseEvent e) {
				getMousePos(e);

				// je�li menu widoczne
				if (state == State.MENU)
					// to przeka� mu zdarzenie
					menu.mouseMoved(e);
			}
			
			private void getMousePos(MouseEvent e) {
				// pocz�tek wy�wietlania jest ustawiony na �rodku okienka
				// wi�c odczytany x,y te� musimy przesun�� wzgl�dem �rodka panelu
				int tX = e.getX() - renderingPanel.getWidth()/2;
				int tY = e.getY() - renderingPanel.getHeight()/2;
				
				// mamy ju� odpowiednie warto�ci wi�c zapisujemy je na potem 
				if (lastMousePos == null)
					lastMousePos = new Point<Integer>(tX, tY);
				else
					lastMousePos.set(tX, tY);
			}
		};
		renderingPanel.addMouseListener(mouseAdapter);
		renderingPanel.addMouseMotionListener(mouseAdapter);
		
		renderingPanel.addKeyListener(new KeyAdapter() {
			@Override public void keyPressed(KeyEvent e) {
				switch (e.getKeyCode()) {
				case KeyEvent.VK_ESCAPE: {
					if (state == State.PLAYING)
						showMenu();
				} break;
				}
			}
		});

		world = new World(renderingPanel.getWidth(), renderingPanel.getHeight());
		menu = new Menu(10,10, 60,50);

		// stworzenie przycisku start
		menu.add( new Button("Start", 5,5, 50,20) {
			@Override public void leftMouseClick(MouseEvent e) {
				newGame(); // uruchomienie gry
			}
		});
		
		showMenu();
	}
	
	private void newGame() {
		// usuni�cie poprzednich wyst�pie� obiekt�w
		world.clearEntities();
		score = 0;
		
		// dodanie standardowych obiekt�w
		// obiekt gracza
		player = new Player();
		player.setPos(-300, 0);
		world.addEntity(player);
		// danie graczowi broni
//		Point<Integer> point = player.getModel().get("forearmL").getPin();
//		player.giveWeapon( new Bow(point.x, point.y, 10) );
		player.giveWeapon( new Bow(0, 0, 10) );
		world.addEntity(player.weapon);
		
		// obiekt przeciwnika
		world.addEntity(new Target(300, 0));
//		addEntity(new Arrow(0, 0, 50, 4, 30));
		
		// uruchomienie silnika gry
		state = State.PLAYING;
	}
	
	private void showMenu() {
		menu.init();
		state = State.MENU;
	}
	
	public void update() {
		// je�li gra jest uruchomiona
		if (state == State.PLAYING)
			((Updateable)world).update();
	}

	public void render(Graphics2D g) {
		AffineTransform savedTransform = g.getTransform();
		
		// je�li jeste�my w menu
		if (state == State.MENU)
			// to je wy�wietl
			((Renderable)menu).render(g);
		else {
			// w przeciwnym wypadku wy�wietl renderuj gr�
			((Renderable)world).render(g);
			// i wy�wietl punkty gracza
			g.setTransform(savedTransform);
			g.setColor(Color.BLACK);
			g.drawString("Score: "+score, 10, 20);
		}
	};
	
	
	
	//## wyliczenie opisuj�ce aktualny stan gry
	public enum State {
		MENU,
		PLAYING
	}
}
