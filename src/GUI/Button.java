package GUI;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.RoundRectangle2D;

import engine.utils.Clickable;
import engine.utils.Point;
import engine.utils.Renderable;
import engine.utils.Updateable;

public class Button extends Component implements Updateable, Renderable, Clickable {
	
	private String text;
	
	private Color background;
	private Color color;
	
	private boolean hovering;
	
	// konstruktory
	public Button(String text, int x, int y, int width, int height, Color c, Color b) {
		super(x, y, width, height);
		this.text = text;
		this.color = c;
		this.background = b;
		this.hovering = false;
	}
	public Button(String text, int x, int y, int width, int height, Color b) {
		this(text, x, y, width, height, Color.BLACK, b);
	}
	public Button(String text, int x, int y, int width, int height) {
		this(text, x, y, width, height, Color.BLACK, new Color(.7f, .7f, .7f, .75f));
	}
	
	@Override
	public boolean pointInside(Point<Integer> point) {
		AffineTransform transform = new AffineTransform();
		transform.translate(pos.x, pos.y);
		return transform.createTransformedShape(getShape()).contains(point.x, point.y);
	}
	
	public Shape getClip() {
		return new Area(new RoundRectangle2D.Float(0, 0, size.x+1, size.y+1, size.y, size.y));
	}
	
	public Shape getShape() {
		return new Area(new RoundRectangle2D.Float(0, 0, size.x, size.y, size.y, size.y));
	}

	@Override
	public void render(Graphics2D g) {
		Shape oldClip = g.getClip();
		
		g.translate(pos.x, pos.y);
		g.clip(getClip());
		
		// fill area
		Shape shape = getShape();
		
		if (hovering)
			g.setColor(background.brighter());
		else
			g.setColor(background);
		g.fill(shape);
		
		// draw text
		g.setColor(color);
		g.drawString(text, size.x/2 - g.getFontMetrics().stringWidth(text)/2, size.y/2 + g.getFontMetrics().getHeight()/4);
		
		// draw outline
		g.draw(shape);
		g.setClip(oldClip);
	}

	@Override
	public void update() {
		
	}

	@Override
	public void onMouseEnter() {
		hovering = true;
	}
	
	@Override
	public void onMouseExit() {
		hovering = false;
	}

	// handling left and right mouse event
	@Override
	public void onClick(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON1)
			leftMouseClick(e);
		else if (e.getButton() == MouseEvent.BUTTON3)
			rightMouseClick(e);
	}
	
	// to override on instance creating
	public void  leftMouseClick(MouseEvent e) { }
	public void rightMouseClick(MouseEvent e) { }
}
