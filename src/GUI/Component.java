package GUI;

import java.awt.Rectangle;

import engine.utils.Point;

public class Component {
	protected Point<Integer> pos;
	protected Point<Integer> size;
	
	public Component(int x, int y, int width, int height) {
		this.pos = new Point<Integer>(x, y);
		this.size = new Point<Integer>(width, height);
	}

	public boolean pointInside(Point<Integer> point) {
		return new Rectangle(pos.x, pos.y, size.x, size.y).contains(point.x, point.y);
	}

	// do nadpisania w klasach dziedziczących
	public void onMouseEnter() {}
	public void onMouseExit() {}
}
