package GUI;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.util.Iterator;
import java.util.LinkedList;

import engine.utils.Clickable;
import engine.utils.Point;
import engine.utils.Renderable;
import engine.utils.Updateable;

public class Menu implements Updateable, Renderable{
	
	private Point<Integer> pos;
	private Point<Integer> size;
	
	private LinkedList<Component> components;
	
	public Menu(int x, int y, int width, int height) {
		// inicjalizacja zmiennych
		pos = new Point<>(x, y);
		size = new Point<>(width, height);
		components = new LinkedList<>();
	}
	
	// dodanie komponentu do menu
	public void add(Component component) {
		components.add(component);
	}
	
	public void init() {
		// clear flags for all components
		for (Component component : components)
			component.onMouseExit();
	}

	@Override
	public void render(Graphics2D g) {
		Shape oldClip = g.getClip();
		Shape shape = new Rectangle(0, 0, size.x, size.y);
		
		// przesu� si� na pozycj� menu i ustaw ograniczenie rysowania tylko wewn�trz menu
		g.translate(pos.x, pos.y);
		g.clip(new Rectangle(0, 0, size.x+1, size.y+1));
		
		// rysuj t�o i obramowanie menu
		g.setColor(new Color(0, 0, 0, .5f));
		g.fill(shape);
		g.setColor(Color.BLACK);
		g.draw(shape);

		// renderowanie element�w menu
		AffineTransform savedTransform = g.getTransform();
		for (Component component : components)
			if (component instanceof Renderable) {
				((Renderable)component).render(g);
				g.setTransform(savedTransform);
			}
		
		g.setClip(oldClip);
	}

	@Override
	public void update() {
		// aktualizuj stan komponent�w
		for (Component component : components)
			// ale tylko tych, kt�re wolno aktualizowa�
			if (component instanceof Updateable)
				((Updateable)component).update();
	}
	
	
	//## input handling // mouse, keyboard
	public void mouseMoved(MouseEvent e) {
		// sprawd� czy mysz jest wewn�trz menu i je�li nie to wyjd� z funkcji
		if (! new Rectangle(pos.x, pos.y, size.x, size.y).contains(e.getX(), e.getY())) return;
		
		// skoro punkt jest wewn�trz menu, to wyznacz warto�ci relatywne do pozycji menu (lewy g�rny r�g menu, to punkt {0,0})
		int X = e.getX() - pos.x;
		int Y = e.getY() - pos.y;
		
		// iteracja od ostatniego elementu do pierwszego (ostatni element jest najbardziej na wierzchu)
		Iterator<Component> iterator = components.descendingIterator();
		Component component;
		while (iterator.hasNext()) {
			// pobierz nast�pny komponent
			component = iterator.next();
			// sprawd� czy punkt znajduje si� w obszarze komponentu
			if (component.pointInside(new Point<Integer>(X, Y))) {
				// je�li tak to wywo�aj zdarzenie wej�cia do obszaru komponentu
				component.onMouseEnter();
			} else {
				// je�li nie to wywo�aj zdarzenie wyj�cia z obszaru komponentu
				component.onMouseExit();
			}
		}
	}
	
	public void mousePressed(MouseEvent e) {
		// sprawd� czy klikni�to wewn�trz menu i je�li nie to wyjd� z funkcji
		if (! new Rectangle(pos.x, pos.y, size.x, size.y).contains(e.getX(), e.getY())) return;

		// skoro punkt jest wewn�trz menu, to wyznacz warto�ci relatywne do pozycji menu (lewy g�rny r�g menu, to punkt {0,0})
		int X = e.getX() - pos.x;
		int Y = e.getY() - pos.y;
		
		// iteracja od ostatniego elementu do pierwszego (ostatni element jest najbardziej na wierzchu)
		Iterator<Component> iterator = components.descendingIterator();
		Component component;
		while (iterator.hasNext()) {
			// pobierz nast�pny komponent
			component = iterator.next();
			// sprawd� czy komponent mo�na klikn��
			if (component instanceof Clickable) {
				// sprawd� czy klikni�to w obszarze komponentu
				if (component.pointInside(new Point<Integer>(X, Y))) {
					((Clickable)component).onClick(e); // wywo�aj klikni�cie
					break; // wyjd� z p�tli, nie klikamy element�w pod tym komponentem
				}
			}
		}
	}
	
	public void keyPressed(KeyEvent e) {
		
	}
}
