package GUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.UIManager;

import engine.Engine;

@SuppressWarnings("serial")
public class Main extends JFrame {
	
	//# funkcja uruchamiaj�ca program (jedyna w aplikacji)
	public static void main(String[] args) {
		// ustawienie sk�rki aplikacji na windowsowsk�
		try {
			UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
		
		// stworzenie instancji programu
		getInstance();
	}
	
	
	//# klasa z mo�liw� tylko jedn� instancj�
	private static Main instance = null;
	public static Main getInstance() {
		if (instance == null)
			instance = new Main();
		return instance;
	}
	
	
	//# rzeczy dotycz�ce okienka gry	
	public JPanel renderingPanel;
	
	protected Main() {
		setTitle("Projekt - Paulina Urban");
		setSize(800, 600);
		setResizable(false); // blokowanie zmiany rozmiaru okna
		setLayout(null); // sprawienie, �eby dodawane komponenty pojawia�y si� we wskazanej pozycji
		setLocationRelativeTo(null); // wy�wietlenie okienka na �rodku pulpitu
		setDefaultCloseOperation(EXIT_ON_CLOSE); // po zamkni�ciu okienka wyjd� z programu
		
		// panel s�u��cy do rysowania gry
		renderingPanel = new JPanel() {
			@Override protected void paintComponent(Graphics g) {
				Graphics2D g2 = (Graphics2D)g;
				
				// w��czenie wyg�adzania kraw�dzi
				g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
				
				// wype�nienie bia�ym kolorem
				g2.setColor(Color.WHITE);
				g2.fill(getBounds());
				
				AffineTransform savedTransform = g2.getTransform();
				// wyrenderowanie gry
				Engine.getInstance().render(g2);
				g2.setTransform(savedTransform);
			}
		};
		renderingPanel.setBounds(0, 0, getWidth(), getHeight());
		renderingPanel.setFocusable(true);
		getContentPane().add(renderingPanel);

		// stworzenie instancji silnika gry i inicjalizacja obiekt�w, przypisanie zdarze� myszy do obiektu renderingPanel
		Engine.getInstance().init(renderingPanel);
		
		// timer s�u��cy do aktualizacji stanu gry i od�wie�ania panelu do renderowania
		Timer timer = new Timer(30, new ActionListener() {
			@Override public void actionPerformed(ActionEvent e) {
				// aktualizacja stanu obiekt�w i wyrenderowanie ich
				Engine.getInstance().update();
				renderingPanel.repaint();
				
				// powiedzenie wirtualnej maszynie javy, �e chcieliby�my pozby� si� nieu�ywanych ju� obiekt�w i zwolni� pami��
				System.gc();
			}
		});
		setVisible(true);
		renderingPanel.requestFocus();
		timer.start(); // uruchomienie timer'a po pojawieniu si� okna
	}
}
